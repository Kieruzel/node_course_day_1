## Event emitter ##

1. Create instance of ```Event Emitter``` class called Timer
2. Then create a Timer instance that will react to certain events 
- start timer
- pause timer
- resume timer
- close timer
- restart counter
3. Emit events tick and tock alternatly each second.
4. Start timer in class constructor
5. Test your timer same as in the commnet below

<!-- 
const timer = new Timer()

setTimeout(function() {
    //pause timer
}, 3000)

setTimeout(function() {
    //resume timer
}, 7000)

setTimeout(function() {
    //pause timer
}, 9000)

setTimeout(function() {
    //restart timer
}, 11000)


setTimeout(function() {
    //clouse timer
}, 15000)
 -->