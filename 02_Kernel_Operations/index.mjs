import https from "https";

const MAX_RETRIES = 22;

const now = Date.now();

for (let i = 0; i < MAX_RETRIES; i++) {
  try {
    https.get("https://www.google.com", function (res) {
      res.on("data", (data) => {});

      res.on("end", () => {
        console.log("Experiment", i, Date.now() - now);
      });
    });
  } catch (err) {
    console.log(err);
  }
}
