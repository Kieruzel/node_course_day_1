// Node.js script

const { TextEncoder, TextDecoder } = require('util');

// Create a function to write text to a SharedArrayBuffer
function writeTextToSharedBuffer(text) {
  // Convert the text to a Uint8Array (binary format)
  const encoder = new TextEncoder();
  const encodedText = encoder.encode(text);

  // Create a SharedArrayBuffer with the same length as the encoded text
  const sharedBuffer = new SharedArrayBuffer(encodedText.length);

  // Create a Uint8Array view on the SharedArrayBuffer
  const view = new Uint8Array(sharedBuffer);

  // Copy the encoded text to the SharedArrayBuffer
  view.set(encodedText);

  return sharedBuffer;
}

// Create a function to read text from a SharedArrayBuffer
function readTextFromSharedBuffer(sharedBuffer) {
  // Create a Uint8Array view on the SharedArrayBuffer
  const view = new Uint8Array(sharedBuffer);

  // Convert the binary data back to text
  const decoder = new TextDecoder();
  const text = decoder.decode(view);

  return text;
}

// Write text to the SharedArrayBuffer
const text = 'Hello, SharedArrayBuffer!';
const sharedBuffer = writeTextToSharedBuffer(text);
console.log('SharedArrayBuffer created and text written.');

// Read text from the SharedArrayBuffer
const readText = readTextFromSharedBuffer(sharedBuffer);
console.log('Text read from SharedArrayBuffer:', readText);