## Thread Pool ##

1. Examine the function pbkdf2Sync execution 
2. Try to guess how many thread this function uses
3. Change the function to its asynchornus version and check execution time again
4. Try to play with ```process.env.UV_THREADPOOL_SIZE``` to increase performance