const http = require('http');
const https = require('https');
const url = require('url');
const fs = require('fs');
const cheerio = require('cheerio');
const path = require('path');

function downloadAndSaveAllLinks(pageUrl) {
  if (!fs.existsSync('files')) {
    fs.mkdirSync('files');
  }

  const protocol = url.parse(pageUrl).protocol === 'https:' ? https : http;
  protocol.get(pageUrl, (response) => {
    if (response.statusCode !== 200) {
      return console.error('Failed to download page:', response.statusCode);
    }

    let data = '';
    response.on('data', (chunk) => {
      data += chunk;
    });

    response.on('end', () => {
      const $ = cheerio.load(data);
      $('a').each((index, element) => {

        if(index > 300) {
            return;
        }

        const link = $(element).attr('href');
        if (link) {
          const linkUrl = url.resolve(pageUrl, link);
          const filename = path.join(__dirname, 'files', new Date().getTime().toString() + '-' + path.basename(url.parse(linkUrl).pathname) + '.html');

          const linkProtocol = url.parse(linkUrl).protocol === 'https:' ? https : http;
          linkProtocol.get(linkUrl, (response) => {
            if (response.statusCode !== 200) {
              return console.error('Failed to download link:', link, response.statusCode);
            }

            let content = '';
            response.on('data', (chunk) => {
              content += chunk;
            });

            response.on('end', () => {
              fs.writeFile(filename, content, (err) => {
                if (err) {
                  return console.error('Error saving to file:', filename, err);
                }
                console.log('Saved:', filename);
              });
            });
          }).on('error', (err) => {
            console.error('Error downloading link:', link, err);
          });
        }
      });
    });
  }).on('error', (err) => {
    console.error('Error downloading page:', err);
  });
}

// Replace the URL below with the URL of the page you want to download links from
downloadAndSaveAllLinks("https://en.wikipedia.org/wiki/Romania");
