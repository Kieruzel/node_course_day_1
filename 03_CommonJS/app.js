// Import the fileUtils module
const extension = process.argv[2];


const fileTxtUtils = require("./fileTxtUtils");
const fileJsUtils = require("./fileJsUtils");
const executionCount = require("./fileTxtUtils").executionCount;





//Checj cache – if the module has already been loaded, it will be returned from the cache
// console.log(require.cache);

// Define the directory and file extension to work with
const directoryPath = __dirname; // Current directory
const targetExtension = "txt";

// Use the listFilesInDirectory function from the fileUtils module
fileTxtUtils.listFilesInDirectory(directoryPath, (err, files) => {
  if (err) {
    console.error(`Error listing files: ${err.message}`);
  } else {
    console.log(`Files with the '${targetExtension}' extension:`);
    executionCount.number++;
    files.forEach((file, index) => {
      console.log(`${index + 1}: ${file}`);
    });

    // This is just a little trick to show that we can change the value in the module that we've imported
    const count = require('./checkCount');
    console.log("Count", count)
  }
});
