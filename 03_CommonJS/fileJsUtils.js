const fs = require('fs');
const path = require('path');

const executionCount = {number: 0}

// Function to list files in a directory with a specific extension
function listFilesInDirectory(dirPath, callback) {
  fs.readdir(dirPath, (err, files) => {
    if (err) {
      return callback(err);
    }

    const filteredFiles = files.filter((file) => {
      return path.extname(file) === `.js`;
    });

    callback(null, filteredFiles);
  });
}

// Export the function so it can be used in other modules
module.exports = {
  listFilesInDirectory,
  executionCount
};