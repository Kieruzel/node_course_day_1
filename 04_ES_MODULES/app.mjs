// Import the listFilesInDirectory function from the fileUtils module
import { listFilesInDirectory } from './fileUtils.mjs';

// import { fileURLToPath } from 'url'
// import { dirname } from 'path'
// const __filename = fileURLToPath(import.meta.url)
// const __dirname = dirname(__filename)

// Define the directory and file extension to work with
const directoryPath = new URL('.', import.meta.url).pathname; // Current directory
const targetExtension = 'txt';

// Use the listFilesInDirectory function from the fileUtils module
listFilesInDirectory(directoryPath, targetExtension)
  .then((files) => {
    console.log(`Files with the '${targetExtension}' extension:`);
    files.forEach((file, index) => {
      console.log(`${index + 1}: ${file}`);
    });
  })
  .catch((err) => {
    console.error(`Error listing files: ${err.message}`);
  });