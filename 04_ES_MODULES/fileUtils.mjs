import fs from "fs/promises";
import path from "path";

// Function to list files in a directory with a specific extension
async function listFilesInDirectory(dirPath, fileExtension) {
  try {
    const files = await fs.readdir(dirPath);
    const filteredFiles = files.filter((file) => {
      return path.extname(file) === `.${fileExtension}`;
    });
    return filteredFiles;
  } catch (err) {
    throw err;
  }
}

// Export the function so it can be used in other modules
export { listFilesInDirectory };
